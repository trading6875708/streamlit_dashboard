# Streamlit Dashboard for Live Services Data Visualisation

This microservice purpose is to show the live data (lives in memory) of the live services' microservice,
it retrieve the data for each ohlcv candles graph/ticker using the GRPC protocol (with the help of pickle library)
![OHLCV Dashboard](ohlcv_streamlit.gif)






