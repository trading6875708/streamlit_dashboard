import streamlit as st
import plotly.graph_objects as go
from core.indicators import twin_range_filter
from core.indicators.supertrend import calculate_supertrend
from core.indicators.average_directional_index import calculate_average_directional_index

def add_twin_range_filter(fig, ohlcv_df, dataframe):
    per1 = st.number_input(f"Enter per1 ({dataframe})", value=27)
    mult1 = st.number_input(f"Enter mult1 ({dataframe})", value=1.6)
    per2 = st.number_input(f"Enter per2 ({dataframe})", value=55)
    mult2 = st.number_input(f"Enter mult2 ({dataframe})", value=2.0)
    df = ohlcv_df.copy()
    df["twin_range_filter"] = twin_range_filter.calculate_twin_range_filter(df, per1, mult1, per2, mult2)
    
    fig.add_trace(go.Scatter(
        x=df.index, 
        y=df["twin_range_filter"], 
        mode='lines', 
        name='Twin Range Filter'
    ))
    return fig

def add_supertrend(fig, ohlcv_df, dataframe):
    try:
        # Get user input for Supertrend parameters
        length = st.number_input(f"Enter Supertrend length ({dataframe})", value=7)
        multiplier = st.number_input(f"Enter Supertrend multiplier ({dataframe})", value=2.0)
        
        ohlcv_df = calculate_supertrend(ohlcv_df, length, multiplier)
        supertrend_column_name = f"SUPERT_{length}_{float(multiplier)}"
        
        fig.add_trace(go.Scatter(
            x=ohlcv_df.index,
            y=ohlcv_df[supertrend_column_name],
            mode='lines',
            name='Supertrend'
        ))
        
    except ValueError as e:
        st.error(f"Error calculating Supertrend: {e}")
    return fig

def add_adx(fig, ohlcv_df, dataframe):
    try:
        # Get user input for ADX parameters
        length = st.number_input(f"Enter ADX length ({dataframe})", value=14)
        
        # Calculate ADX, DMP, and DMN
        ohlcv_df = calculate_average_directional_index(ohlcv_df, length)
        adx_column_name = f"ADX_{length}"
        dmp_column_name = f"DMP_{length}"
        dmn_column_name = f"DMN_{length}"
        
        # Add ADX line to the figure
        fig.add_trace(go.Scatter(
            x=ohlcv_df.index,
            y=ohlcv_df[adx_column_name],
            mode='lines',
            name='ADX'
        ))
        
        # Add DMP line to the figure
        fig.add_trace(go.Scatter(
            x=ohlcv_df.index,
            y=ohlcv_df[dmp_column_name],
            mode='lines',
            name='DMP',
            line=dict(dash='dash')  # Optional: make the line dashed
        ))
        
        # Add DMN line to the figure
        fig.add_trace(go.Scatter(
            x=ohlcv_df.index,
            y=ohlcv_df[dmn_column_name],
            mode='lines',
            name='DMN',
            line=dict(dash='dot')  # Optional: make the line dotted
        ))
        
    except ValueError as e:
        st.error(f"Error calculating ADX: {e}")
    return fig
