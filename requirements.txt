streamlit==1.36.0
grpcio==1.62.0
grpcio-tools==1.62.0
urllib3==1.26.15
python-dotenv==1.0.0
SQLAlchemy==1.4.51
SQLAlchemy-Utils==0.41.1
greenlet==3.0.3
psycopg2-binary
plotly==5.22.0
debugpy

# for calculating the indicators when displaying
pandas==2.0.3
pandas_ta==0.3.14b0
scipy==1.10.1
hmmlearn


# for getting the supported timeframes of each exchange
ccxt==4.3.91

