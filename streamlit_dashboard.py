import os

os.environ["STREAMLIT_BROWSER_GATHERUSAGESTATS"] = "false"

import streamlit as st
import asyncio
import pandas as pd
from core.models.order import Order
from config import engine
from live_services_grpc_client import get_ticker_data, get_symbol_ohlcv
from sqlalchemy import select



# st.sidebar.page_link("pages/balances.py", label="Balances")

# Synchronous wrapper for the asynchronous function
def fetch_ticker_data(exchange, symbols):
    return asyncio.run(get_ticker_data(exchange, symbols))




# Fetch and display ticker data
try:
    ticker_data = fetch_ticker_data("kraken", ["SOL/USDT"])
    st.write(ticker_data)
    ticker_data = fetch_ticker_data("binance", ["USDC/USDT"])
    st.write(ticker_data)
except Exception as e:
    st.error(f"An error occurred while fetching ticker data: {e}")

query = str(Order.__table__.select())
with engine.connect() as conn:
    df = pd.read_sql(query, con=conn.connection)
    st.write(df)
    df['filled_price'] = df['filled'] * df['price']
    grouped_df = df.groupby(['symbol', 'exchange', 'side']).agg({'filled_price': 'sum', 'filled': 'sum'}).reset_index()
    grouped_df['average_filled_price'] = grouped_df['filled_price'] / grouped_df['filled']
    
    buy_df = grouped_df[grouped_df['side'] == 'buy'][['symbol', 'exchange', 'average_filled_price']].rename(columns={'average_filled_price': 'buy_filled_price'})
    sell_df = grouped_df[grouped_df['side'] == 'sell'][['symbol', 'exchange', 'average_filled_price']].rename(columns={'average_filled_price': 'sell_filled_price'})
    merged_df = pd.merge(buy_df, sell_df, on=['symbol', 'exchange'], how='outer')
    st.write(merged_df)


