FROM python:3.9-slim

# Set the working directory in the container to /app
WORKDIR /app

# Prevents Python from writing pyc files.
ENV PYTHONDONTWRITEBYTECODE=1

# Keeps Python from buffering stdout and stderr to avoid situations where
# the application crashes without emitting any logs due to buffering.
ENV PYTHONUNBUFFERED=1

ENV PYTHONPATH="${PYTHONPATH}:/app/:/app/core/:/app/live_services/:/app/protos/"

RUN apt-get update && apt-get install -y \
    build-essential \
    curl \
    software-properties-common \
    git \
    && rm -rf /var/lib/apt/lists/*


# Copy only the requirements.txt first, for better cache on rebuild
COPY requirements.txt /app/

RUN pip3 install -r requirements.txt

EXPOSE 8501

HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health

ENTRYPOINT ["streamlit", "run", "streamlit_dashboard/streamlit_dashboard.py", "--server.port=8501", "--server.address=0.0.0.0"]