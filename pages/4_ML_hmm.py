import asyncio
import streamlit as st
from live_services_grpc_client import get_symbol_ohlcv
import plotly.graph_objects as go
from core.indicators import twin_range_filter
from indicator_graph_util import add_twin_range_filter, add_supertrend, add_adx
from core.indicators.supertrend import supertrend_indicator
from core.indicators.average_directional_index import calculate_average_directional_index
from core.globals import exchange_ohlcv_live_book_symbols_config
import numpy as np
import pandas as pd
from hmmlearn import hmm
from sklearn.preprocessing import StandardScaler

# Initialize the loop
loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


from core.exchanges_clients.ccxt_clients import ccxtClientsDict

def fetch_ohlcv_data(exchange, symbol):
    return asyncio.run(get_symbol_ohlcv(exchange, symbol))

def process_and_display_timeframe(ohlcv_data, timeframe, symbol, exchange):
    ohlcv_df = ohlcv_data.get_symbol_ohlcv_df(timeframe)
    ohlcv_df = ohlcv_df[~ohlcv_df.index.duplicated(keep='first')]
    
    # Calculate returns and trend indicators
    ohlcv_df['Returns'] = ohlcv_df['close'].pct_change()
    ohlcv_df['MA_short'] = ohlcv_df['close'].rolling(window=5).mean()
    ohlcv_df['MA_long'] = ohlcv_df['close'].rolling(window=20).mean()
    ohlcv_df['Trend'] = ohlcv_df['MA_short'] - ohlcv_df['MA_long']
    ohlcv_df['Volatility'] = ohlcv_df['Returns'].rolling(window=20).std()
    
    # Prepare features for HMM
    features = ['Returns', 'Trend', 'Volatility']
    X = ohlcv_df[features].dropna().values
    
    # Normalize the input data
    scaler = StandardScaler()
    X_scaled = scaler.fit_transform(X)
    
    # Define and fit the HMM with more states and iterations
    model = hmm.GaussianHMM(n_components=5, covariance_type="full", n_iter=2000, random_state=42)
    model.fit(X_scaled)
    
    # Predict the hidden states
    hidden_states = model.predict(X_scaled)
    ohlcv_df = ohlcv_df.iloc[len(ohlcv_df)-len(hidden_states):]  # Align dataframe with predictions
    ohlcv_df['Hidden_States'] = hidden_states
    
    # Interpret states based on their characteristics
    state_means = model.means_
    state_interpretations = interpret_states(state_means)
    
    ohlcv_df['Trend_State'] = ohlcv_df['Hidden_States'].map(state_interpretations)
    
    # Create and display price and trend states plot
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=ohlcv_df.index, y=ohlcv_df['close'], mode='lines', name='Close Price', line=dict(color='blue', width=2)))
    
    colors = {'Strong Uptrend': 'green', 'Weak Uptrend': 'lightgreen', 'Neutral': 'yellow', 'Weak Downtrend': 'orange', 'Strong Downtrend': 'red'}
    for state in colors.keys():
        state_data = ohlcv_df[ohlcv_df['Trend_State'] == state]
        fig.add_trace(go.Scatter(x=state_data.index, y=state_data['close'], mode='markers', 
                                 marker=dict(size=8, color=colors[state], symbol='circle'), 
                                 name=f'{state} State'))
    
    fig.update_layout(
        title=f"{symbol} Price and Trend States (HMM) - {timeframe} on {exchange}",
        xaxis_title="Date",
        yaxis_title=f"Price ({symbol.split('/')[1]})",
        legend_title="Legend",
        template="plotly_dark",
        height=600,
        width=1000
    )
    st.plotly_chart(fig)
    
    # Display state analysis
    st.write("Trend State Analysis:")
    for state in ['Strong Uptrend', 'Weak Uptrend', 'Neutral', 'Weak Downtrend', 'Strong Downtrend']:
        state_data = ohlcv_df[ohlcv_df['Trend_State'] == state]
        st.write(f"{state} State:")
        st.write(f"  Mean return: {state_data['Returns'].mean():.4f}")
        st.write(f"  Mean trend: {state_data['Trend'].mean():.4f}")
        st.write(f"  Frequency: {len(state_data) / len(ohlcv_df):.2%}")
        st.write(f"  Average duration: {state_data.index.to_series().diff().mean()}")
        st.write("---")

    # Display the dataframe with new columns
    st.write(ohlcv_df)

def interpret_states(state_means):
    # Sort states based on their trend values
    sorted_states = sorted(enumerate(state_means[:, 1]), key=lambda x: x[1])
    state_interpretations = ['Strong Downtrend', 'Weak Downtrend', 'Neutral', 'Weak Uptrend', 'Strong Uptrend']
    return {sorted_states[i][0]: state_interpretations[i] for i in range(5)}

st.header("live ohlcv graphs")
# Create a dropdown for selecting the exchange
exchange = st.selectbox("Select Exchange", list(exchange_ohlcv_live_book_symbols_config.keys()))


# Create a multiple selection for selecting the symbols based on the selected exchange
if exchange:
    symbols = list(exchange_ohlcv_live_book_symbols_config[exchange].keys())
    selected_symbols = st.multiselect("Select Symbols", symbols)
    if selected_symbols:
        st.write(f"You selected {', '.join(selected_symbols)} on {exchange}")
        
        # Get the supported timeframes for the selected exchange
        exchange_client = ccxtClientsDict[exchange]
        supported_timeframes = list(exchange_client.timeframes.keys()) if hasattr(exchange_client, 'timeframes') else []
        
        st.write(f"Supported timeframes for {exchange}: {', '.join(supported_timeframes)}")
        for symbol in selected_symbols:
            st.subheader(f"OHLCV Data for {symbol} on {exchange}")
            try:
                ohlcv_data = fetch_ohlcv_data(exchange, symbol)
                available_dataframes = exchange_ohlcv_live_book_symbols_config[exchange][symbol]
                # Function to process and display data for a single timeframe
                

                # Process all available timeframes
                available_dataframes = exchange_ohlcv_live_book_symbols_config[exchange][symbol]
                for timeframe in available_dataframes:
                    st.subheader(f"Analysis for {timeframe}")
                    process_and_display_timeframe(ohlcv_data, timeframe, symbol, exchange)

            except Exception as e:
                st.error(f"Error fetching data for {symbol} on {exchange}: {e}")
