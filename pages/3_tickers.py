import asyncio
import streamlit as st
from core.globals import exchange_ticker_symbols
from live_services_grpc_client import get_ticker_data, get_symbol_ohlcv

# Create a dropdown for selecting the exchange
exchange = st.selectbox("Select Exchange", list(exchange_ticker_symbols.keys()), key="selected_exchange")

# Synchronous wrapper for the asynchronous function
def fetch_ticker_data(exchange, symbols):
    return asyncio.run(get_ticker_data(exchange, symbols))

# Create a multiple selection for selecting the traded symbols based on the selected exchange
if exchange:
    traded_symbols = list(exchange_ticker_symbols[exchange])
    selected_symbols = st.multiselect("Select Traded Symbols", traded_symbols, key="selected_symbols")

    if selected_symbols:
        st.write(f"You selected {', '.join(selected_symbols)} on {exchange}")
        ticker_data = fetch_ticker_data(exchange, selected_symbols)
        st.write(ticker_data)
