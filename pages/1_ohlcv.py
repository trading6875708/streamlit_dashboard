import asyncio
import streamlit as st
from live_services_grpc_client import get_symbol_ohlcv
import plotly.graph_objects as go
from core.indicators import twin_range_filter
from indicator_graph_util import add_twin_range_filter, add_supertrend, add_adx, add_volumatic_vidya
from core.indicators.supertrend import supertrend_indicator
from core.indicators.average_directional_index import calculate_average_directional_index
import pandas as pd


# Initialize the loop
loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


from core.exchanges_clients.ccxt_clients import ccxtClientsDict

def fetch_ohlcv_data(exchange, symbol):
    return asyncio.run(get_symbol_ohlcv(exchange, symbol))

from core.globals import exchange_ohlcv_live_book_symbols_config

st.header("live ohlcv graphs")
# Create a dropdown for selecting the exchange, defaulting to binance
exchange = st.selectbox("Select Exchange", list(exchange_ohlcv_live_book_symbols_config.keys()), index=list(exchange_ohlcv_live_book_symbols_config.keys()).index('binance'))


# Create a multiple selection for selecting the symbols based on the selected exchange
if exchange:
    symbols = list(exchange_ohlcv_live_book_symbols_config[exchange].keys())
    selected_symbols = st.multiselect("Select Symbols", symbols)
    # Update the indicator options to include Supertrend
    indicator_options = ["Twin Range Filter", "Supertrend", "ADX", "Volumatic VIDYA", "None"]
    selected_indicator = st.selectbox("Select Indicator", indicator_options, index=indicator_options.index("Supertrend"))
    if selected_symbols:
        st.write(f"You selected {', '.join(selected_symbols)} on {exchange}")
        
        # Get the supported timeframes for the selected exchange
        exchange_client = ccxtClientsDict[exchange]
        supported_timeframes = list(exchange_client.timeframes.keys()) if hasattr(exchange_client, 'timeframes') else []
        
        st.write(f"Supported timeframes for {exchange}: {', '.join(supported_timeframes)}")
        for symbol in selected_symbols:
            st.subheader(f"OHLCV Data for {symbol} on {exchange}")
            try:
                ohlcv_data = fetch_ohlcv_data(exchange, symbol)
                for timeframe in exchange_ohlcv_live_book_symbols_config[exchange][symbol]:
                    ohlcv_df = ohlcv_data.get_symbol_ohlcv_df(timeframe)
                    st.write(f"Timeframe: {timeframe} with {len(ohlcv_df)} rows")
                    num_duplicates = ohlcv_df.duplicated().sum()
                    st.write(f"Number of duplicated rows: {num_duplicates}")
                    st.write(ohlcv_df)
                    
                    # Create plotly figure
                    plotly_fig = go.Figure()

                    # Calculate the y-axis range
                    y_min = ohlcv_df['low'].min()
                    y_max = ohlcv_df['high'].max()
                    y_range = y_max - y_min
                    y_padding = y_range * 0.05  # Add 5% padding

                    # Add candlestick chart to plotly figure
                    plotly_fig.add_trace(go.Candlestick(
                        x=ohlcv_df.index, 
                        open=ohlcv_df['open'], 
                        high=ohlcv_df['high'], 
                        low=ohlcv_df['low'], 
                        close=ohlcv_df['close'],
                        name='OHLCV'
                    ))

                    # Set the y-axis range explicitly
                    plotly_fig.update_layout(
                        yaxis=dict(
                            range=[y_min - y_padding, y_max + y_padding]
                        )
                    )
                    # Make sure the index is datetime
                    ohlcv_df.index = pd.to_datetime(ohlcv_df.index)
                    
                    if selected_indicator == "Twin Range Filter":
                        fig = add_twin_range_filter(plotly_fig, ohlcv_df, timeframe)
                    elif selected_indicator == "Supertrend":
                        fig = add_supertrend(plotly_fig, ohlcv_df, timeframe)
                    elif selected_indicator == "ADX":
                        fig = add_adx(plotly_fig, ohlcv_df, timeframe)
                    elif selected_indicator == "Volumatic VIDYA":
                        fig = add_volumatic_vidya(plotly_fig, ohlcv_df, timeframe)
                    elif selected_indicator == "None":
                        raise ValueError("No indicator selected")
                    # Add more elif conditions for other indicators as needed

                    st.plotly_chart(fig)
            except Exception as e:
                st.error(f"An error occurred while fetching OHLCV data for {symbol} on {exchange}: {e}")
