import plotly.express as px
import streamlit as st
from core.models.exchange_balances import ExchangeBalances
import asyncio
import pandas as pd
from config import engine
from live_services.live_data_helpers import get_usd_rate
from sqlalchemy import select

query = str(ExchangeBalances.__table__.select())
with engine.connect() as conn:
    # Read the data from the database into a DataFrame
    df = pd.read_sql(query, con=conn.connection)
    st.write(df)

    # Define an asynchronous function to add a new column with USD values
    async def add_usd_value_column(df):
        # Gather the exchange rates for all currencies
        rates = await asyncio.gather(
            *[get_usd_rate(exchange, currency) for exchange, currency in zip(df["exchange_name"], df["currency"])]
        )
        # Calculate the USD values
        df["usd_value"] = [rate * total for rate, total in zip(rates, df["total"])]
        return df
    
    # # Run the asynchronous function and update the DataFrame
    df = asyncio.run(add_usd_value_column(df))
    # # Display the updated DataFrame in Streamlit
    # st.write(df)


# Sample data
labels = ["A", "B", "C", "D"]
sizes = [15, 30, 45, 10]

fig = px.pie(names=labels, values=sizes, title="Sample Pie Chart")

# Display the pie chart in Streamlit
st.plotly_chart(fig)
